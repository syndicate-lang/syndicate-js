/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2021 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

export assertion type BoxState(value: number);
export message type SetBox(newValue: number);

export const N = 100000;
