/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2021 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { BoxState, SetBox, N } from './protocol.js';
import { Ref } from '@syndicate-lang/core';

export function boot(ds: Ref) {
    spawn named 'box' {
        field boxValue: number = 0;
        at ds {
            assert BoxState(boxValue.value);
            on message SetBox($v: number) => boxValue.value = v;
        }
        stop on (boxValue.value === N) {
            console.log('terminated box root facet');
        }
    }
}
