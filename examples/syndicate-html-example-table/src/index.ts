/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2021 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Dataspace, Value, Ref } from '@syndicate-lang/core';
import { boot as bootHtml, UIEvent, GlobalEvent, HtmlFragments, template, Anchor } from '@syndicate-lang/html';

assertion type Person(id, firstName, lastName, address, age);
message type SetSortColumn(number);

Dataspace.boot(ds => {
    bootHtml(ds);
    at ds {
        function newRow(id: number, firstName: string, lastName: string, address: string, age: number) {
            spawn named ('model' + id) {
                assert Person(id, firstName, lastName, address, age);
            }
        }

        newRow(1, 'Keith', 'Example', '94 Main St.', 44);
        newRow(2, 'Karen', 'Fakeperson', '5504 Long Dr.', 34);
        newRow(3, 'Angus', 'McFictional', '2B Pioneer Heights', 39);
        newRow(4, 'Sue', 'Donnem', '1 Infinite Loop', 104);
        newRow(5, 'Boaty', 'McBoatface', 'Arctic Ocean', 1);

        spawn named 'view' {
            let ui = new Anchor();
            field orderColumn: number = 2;

            function cell(text: Value): HtmlFragments {
                return template`<td>${text.toString()}</td>`;
            }

            on message SetSortColumn($c: number) => orderColumn.value = c;

            during Person($id: number, $firstName: string, $lastName: string, $address: string, $age: number) => {
                assert ui.context(id).html(
                    'table#the-table tbody',
                    template`<tr>${[id, firstName, lastName, address, age].map(cell)}</tr>`,
                    [id, firstName, lastName, address, age][orderColumn.value]);
            }
        }

        spawn named 'controller' {
            on message GlobalEvent('table#the-table th', 'click', $e) => {
                const event = (e as Ref).target.data as Event;
                send message SetSortColumn(JSON.parse((event.target as HTMLElement).dataset.column!));
            }
        }

        spawn named 'alerter' {
            let ui = new Anchor();
            assert ui.html('#extra', template`<button>Click me</button>`);

            on message UIEvent(ui.fragmentId, '.', 'click', $_e) => {
                alert("Hello!");
            }
        }
    }
});

