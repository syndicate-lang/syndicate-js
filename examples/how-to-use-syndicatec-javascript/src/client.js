/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2021 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { BoxState, SetBox } from './protocol.js';

export function boot(ds, doneCallback) {
  spawn named 'client' {
    at ds {
      on asserted BoxState($v) => send message SetBox(v + 1);
      on retracted BoxState(_) => {
        console.log('box gone');
        doneCallback();
      }
    }
  }
}
