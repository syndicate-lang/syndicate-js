/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2021 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { N } from './protocol.js';
import * as Box from './box.js';
import * as Client from './client.js';
import { Dataspace } from '@syndicate-lang/core';

console.time('box-and-client-' + N.toString());
Dataspace.boot(ds => {
  Box.boot(ds);
  Client.boot(ds, () => console.timeEnd('box-and-client-' + N.toString()));
});
