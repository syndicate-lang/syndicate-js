#!/usr/bin/env -S node -r @syndicate-lang/loader
/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

assertion type BoxState(value);
message type SetBox(newValue);

const N = 100000;

spawn named 'box' {
  field boxValue = 0;

  assert BoxState(boxValue.value);
  on message SetBox($v) => boxValue.value = v;

  stop on (boxValue.value === N) {
    console.log('terminated box root facet');
  }
}

console.time('box-and-client-' + N.toString());
spawn named 'client' {
  on asserted BoxState($v) => send message SetBox(v + 1);
  on retracted BoxState(_) => {
    console.log('box gone');
    console.timeEnd('box-and-client-' + N.toString())
  }
}
