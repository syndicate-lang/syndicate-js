# How to use @syndicate-lang/loader

Running a module:

```shell
node -r @syndicate-lang/loader index.js
```

Running a standalone script:

```shell
./standalone.js
```
