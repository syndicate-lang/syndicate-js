/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

const { BoxState, SetBox, N } = require('./protocol.js');

function boot() {
  spawn named 'box' {
    field boxValue = 0;

    assert BoxState(boxValue.value);
    on message SetBox($v) => boxValue.value = v;

    stop on (boxValue.value === N) {
      console.log('terminated box root facet');
    }
  }
}

exports.boot = boot;
