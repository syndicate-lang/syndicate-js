/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

const { BoxState, SetBox } = require('./protocol.js');

function boot(doneCallback) {
  spawn named 'client' {
    on asserted BoxState($v) => send message SetBox(v + 1);
    on retracted BoxState(_) => {
      console.log('box gone');
      doneCallback();
    }
  }
}

exports.boot = boot;
