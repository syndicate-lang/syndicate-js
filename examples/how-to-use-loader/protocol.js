/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

assertion type BoxState(value);
message type SetBox(newValue);

const N = 100000;

exports.BoxState = BoxState;
exports.SetBox = SetBox;
exports.N = N;
