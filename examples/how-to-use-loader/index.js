/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

const { N } = require('./protocol.js');
const Box = require('./box.js');
const Client = require('./client.js');

console.time('box-and-client-' + N.toString());
Box.boot();
Client.boot(() => console.timeEnd('box-and-client-' + N.toString()));
