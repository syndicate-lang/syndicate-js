# Flappy Bird in Syndicate/js

Inspired by Joshua Cole's 2016 post on
[Flappy Bird in Eve](http://incidentalcomplexity.com/2016/08/23/flappy-eve/),
and taking art resources from
[Bruce Hauman's Clojure-based flappy bird demo](https://github.com/bhauman/flappy-bird-demo),
this is an implementation of a Flappy-Bird-like game for the browser
using Syndicate/js.

![Screenshot](screenshot.png)

## Licence

[GPL-3.0+](gpl-3.0.txt), except the resources in `css` and `imgs`, which are taken
from [Bruce Hauman's flappy bird demo](https://github.com/bhauman/flappy-bird-demo)
which is, itself, under the Eclipse Public Licence version 1.0 or later.
