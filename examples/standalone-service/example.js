#!/usr/bin/env -S node -r @syndicate-lang/loader
/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

const { stringify } = require('@syndicate-lang/core');
const { service } = require('@syndicate-lang/service');

service(args => {
    console.log('+', stringify(args));
    on stop { console.log('-', stringify(args)); }
});
