__ignored__ := $(shell ./setup.sh)

LERNA=./node_modules/.bin/lerna

bootstrap: node_modules/lerna

node_modules/lerna:
	yarn install
	$(MAKE) clean
	+$(MAKE) -j$$(nproc) all

clean.local:

clean: clean.local
	$(LERNA) exec 'yarn clean || true'

veryclean: clean.local
	$(LERNA) exec 'yarn veryclean || true'
	rm -rf node_modules

all:
	$(LERNA) exec yarn prepare

watch:
	inotifytest make -j$$(nproc) all

PROTOCOLS_BRANCH=main
pull-protocols:
	git subtree pull -P packages/core/protocols \
		-m 'Merge latest changes from the syndicate-protocols repository' \
		git@git.syndicate-lang.org:syndicate-lang/syndicate-protocols \
		$(PROTOCOLS_BRANCH)
push-protocols:
	git subtree push -P packages/core/protocols \
		git@git.syndicate-lang.org:syndicate-lang/syndicate-protocols \
		$(PROTOCOLS_BRANCH)

fixcopyright:
	-fixcopyright.rkt --preset-typescript --file-pattern 'packages/**.ts' GPL-3.0-or-later
	-fixcopyright.rkt --preset-javascript --file-pattern 'packages/**.js' GPL-3.0-or-later
