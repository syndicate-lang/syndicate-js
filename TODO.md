
                // // v Ideally, this would be of type "Sturdy.SturdyRef | null", but (1) the
                // // current Dataflow implementation isn't bright enough to mark valuesas being
                // // convertible to preserves values on demand, and (2) null isn't preservesable
                // // in any case. If preserves were improved to be able to convert schema-parsed
                // // values to preserves on demand, and to know it could do that at the type
                // // level, then I could change Dataflow to support any value that could be
                // // converted to preserves on demand, and I could special-case null and
                // // undefined for the ergonomics.
                // field servercap: AnyValue = false;
                // on asserted InputValue('#servercap', $v: string) => {
                //     servercap.value = false;
                //     try {
                //         const a = new Reader<Ref>(v).next();
                //         if (Sturdy.toSturdyRef(a) !== void 0) servercap.value = a;
                //     } catch (e) {
                //         console.error(e);
                //     }
                // }
                // assert UIAttribute('#servercap', 'class', 'invalid') when (!servercap.value);


 - [DONE] `during/spawn`
 - [DONE] `during`
 - [DONE] `let { LaterThan } = activate require("@syndicate-lang/driver-timer");`
 - [DONE] `react`
 - [DONE] `spawn*` or similar - looks like `spawn on start { ... }` will do the trick
 - [DONE] activation
 - [DONE] remove ground dataspace syntax
 - [DONE] `spawn :let childVal = parentCalculation { ... }`
 - [DONE] better autobuilding for smooth and fast dev; babel uses gulp?
 - [DONE] dataspaces, dataspace relays
 - [DONE? Surely there's more] pin down and fix the problems with facet field scope (!!)
 - [DONE] `npm init @syndicate`
 - [DONE] change send syntax from `^ ...` to `send ...` :-(
    - Using `^` is too cute. Indentation doesn't work, and forgetting a semicolon causes silent xor!

 - [DONE] timer driver
 - [DONE] ui driver
 - [DONE] web driver
 - [DONE] tcp driver

 - `defer` statement
 - `define/query`
 - some kind of `stop facet` statement - put a Symbol on the fields blob?

 - other kinds of relay

 - alias wrapExternal to index.js

 - @syndicate-lang/standalone, analogous to @babel/standalone

 - deferTurn should prevent a facet from terminating! This came up in
   some formulations of the game-restart logic in the flappy bird
   demo.

 - driver-streams-node/src/subprocess.js: perhaps figure out some way
   of blocking SIGQUIT, which I'm currently using to get debug output,
   in children, so they don't terminate too?
