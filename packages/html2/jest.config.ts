/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

export default {
  preset: 'ts-jest',
  testEnvironment: './patched-jsdom.mjs',
};
