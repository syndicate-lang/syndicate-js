/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { randomId } from "@syndicate-lang/core";

export function escape(s: string): string {
    return s
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#39;");
}

export type HtmlFragment = string | number | Node | Array<HtmlFragment>;

const tag = randomId(8, true);
const onePlaceholderRe = new RegExp(`x-${tag}-(\\d+)-${tag}-x`);
const allPlaceholdersRe = new RegExp(`x-${tag}-(\\d+)-${tag}-x`, 'g');
function placeholder(n: number): string {
    return `x-${tag}-${n}-${tag}-x`;
}

function splitByPlaceholders(s: string): { constantParts: string[], placeholders: number[] } {
    let match: RegExpExecArray | null = null;
    let lastConstantStart = 0;
    const constantParts: string[] = [];
    const placeholders: number[] = [];
    while ((match = allPlaceholdersRe.exec(s)) !== null) {
        constantParts.push(s.substring(lastConstantStart, match.index));
        placeholders.push(parseInt(match[1], 10));
        lastConstantStart = allPlaceholdersRe.lastIndex;
    }
    constantParts.push(s.substring(lastConstantStart));
    return { constantParts, placeholders };
}

function renderFragment(f: HtmlFragment, escapeStrings: boolean): string[] {
    const result: string[] = [];
    function walk(f: HtmlFragment) {
        if (Array.isArray(f)) {
            f.forEach(walk);
        } else {
            switch (typeof f) {
                case 'string': result.push(escapeStrings? escape(f) : f); break;
                case 'number': result.push('' + f); break;
                default: throw new Error("Cannot render Node in attribute context");
            }
        }
    }
    walk(f);
    return result;
}

function followPath(topNode: ParentNode, path: number[]): Node {
    let n = topNode.childNodes[path[0]];
    for (let j = 1; j < path.length; j++) n = n.childNodes[path[j]];
    return n;
}

interface VariableLocation {
    update(variableParts: HtmlFragment[]): void;
}

interface VariablePlaceholder {
    install(placeholderNode: Node): VariableLocation;
}

class NodeInserter implements VariablePlaceholder {
    constructor(public variablePartIndex: number) {}

    install(placeholderNode: Node): VariableLocation {
        return new NodeInsertionLocation(this.variablePartIndex, placeholderNode);
    }
}

class NodeInsertionLocation implements VariableLocation {
    insertedNodes: Node[];
    referenceNode: ChildNode | null;
    parentNode: ParentNode;

    constructor(public variablePartIndex: number, placeholderNode: Node) {
        this.insertedNodes = [placeholderNode];
        this.referenceNode = placeholderNode.nextSibling;
        this.parentNode = placeholderNode.parentNode!;
    }

    update(variableParts: HtmlFragment[]): void {
        this.insertedNodes.forEach(n => {
            if (n.parentNode === this.parentNode) this.parentNode.removeChild(n);
        });
        this.insertedNodes.length = 0;

        const walk = (f: HtmlFragment) => {
            if (Array.isArray(f)) {
                f.forEach(walk);
            } else {
                switch (typeof f) {
                    case 'string': this.insertedNodes.push(document.createTextNode(f)); break;
                    case 'number': this.insertedNodes.push(document.createTextNode('' + f)); break;
                    case 'object':
                        if (f !== null && 'nodeType' in f) {
                            this.insertedNodes.push(f);
                            break;
                        }
                        /* fall through */
                    default: {
                        let info;
                        try {
                            info = '' + f;
                        } catch (_e) {
                            info = (f as any).toString();
                        }
                        this.insertedNodes.push(
                            document.createTextNode(`<ERROR: invalid HtmlFragment: ${info}>`));
                        break;
                    }
                }
            }
        }
        walk(variableParts[this.variablePartIndex]);

        this.insertedNodes.forEach(n => this.parentNode.insertBefore(n, this.referenceNode));
    }
}

class AttributesInserter implements VariablePlaceholder {
    constructor(public variablePartIndex: number) {}

    install(node: Node): VariableLocation {
        if (!(node instanceof Element)) throw new Error("Cannot attach attributes to non-element");
        return new AttributesInsertionLocation(this.variablePartIndex, node);
    }
}

class AttributesInsertionLocation implements VariableLocation {
    attrNames: string[] = [];

    constructor(public variablePartIndex: number, public node: Element) {}

    update(variableParts: HtmlFragment[]): void {
        this.attrNames.forEach(n => {
            this.node.removeAttribute(n);
            if (n === 'checked' && 'checked' in this.node) this.node.checked = false;
        });
        this.attrNames.length = 0;

        const e = document.createElement('template');
        const v = variableParts[this.variablePartIndex];
        e.innerHTML = `<x-dummy ${renderFragment(v, false).join('')}></x-dummy>`;
        Array.from(e.content.firstElementChild!.attributes).forEach(a => {
            this.attrNames.push(a.name);
            this.node.setAttribute(a.name, a.value);
            if (a.name === 'value' && 'value' in this.node) this.node.value = a.value;
            if (a.name === 'checked' && 'checked' in this.node) this.node.checked = true;
        });
    }
}

type TemplateAttributeValue = {
    attrName: string,
    constantParts: string[],
    placeholders: number[],
};

class AttributeValueInserter implements VariablePlaceholder {
    constructor(public spec: TemplateAttributeValue) {}

    install(node: Node): VariableLocation {
        if (!(node instanceof Element)) {
            throw new Error(`Cannot attach attribute ${this.spec.attrName} to non-element`);
        }
        return new AttributeValueInsertionLocation(this.spec, node);
    }
}

class AttributeValueInsertionLocation implements VariableLocation {
    constructor(public spec: TemplateAttributeValue, public node: Element) {}

    update(variableParts: HtmlFragment[]): void {
        const pieces = [this.spec.constantParts[0]];
        this.spec.placeholders.forEach((n, i) => {
            pieces.push(...renderFragment(variableParts[n], false));
            pieces.push(this.spec.constantParts[i + 1]);
        });
        this.node.setAttribute(this.spec.attrName, pieces.join(''));
        if (this.spec.attrName === 'value' && 'value' in this.node) this.node.value = pieces.join('');
    }
}

class CompoundPlaceholder implements VariablePlaceholder {
    constructor(public placeholders: VariablePlaceholder[]) {}

    install(node: Node): VariableLocation {
        return new CompoundLocation(this.placeholders.map(p => p.install(node)));
    }
}

class CompoundLocation implements VariableLocation {
    constructor(public locations: VariableLocation[]) {}

    update(variableParts: HtmlFragment[]): void {
        this.locations.forEach(l => l.update(variableParts));
    }
}

export class HtmlFragmentInstance {
    nodes: ChildNode[];
    constructor(
        public container: ParentNode,
        public locations: VariableLocation[],
    ) {
        this.nodes = Array.from(this.container.childNodes);
    }

    reset(): Element[] {
        const placeholders = this.nodes.map(_n => makePlaceholder());
        placeholders.forEach((p, i) => {
            const n = this.nodes[i];
            if (n.parentNode === null) return;
            if (n.parentNode === this.container) return;
            n.parentNode.insertBefore(p, n);
            n.parentNode.removeChild(n);
        });
        while (this.container.childNodes.length > 0) { this.container.removeChild(this.container.childNodes[0]); }
        this.nodes.forEach(n => this.container.appendChild(n));
        return placeholders;
    }

    extract(placeholders: Element[]): ChildNode[] {
        const result = Array.from(this.container.childNodes);
        placeholders.forEach((p, i) => {
            const n = this.nodes[i];
            if (p.parentNode === null) return;
            p.parentNode.insertBefore(n, p);
            p.parentNode.removeChild(p);
        });
        return result;
    }

    update(variableParts: HtmlFragment[]): ChildNode[] {
        const placeholders = this.reset();
        this.locations.forEach(l => l.update(variableParts));
        return this.extract(placeholders);
    }
}

function makePlaceholder(): Element {
    return document.createElement('x-placeholder');
}

export class HtmlFragmentBuilder {
    template: HTMLTemplateElement = document.createElement('template');
    placeholderActions: { path: number[], placeholder: VariablePlaceholder }[] = [];

    constructor(constantParts: TemplateStringsArray) {
        const pieces: string[] = [];
        constantParts.raw.forEach((r, i) => {
            if (i > 0) pieces.push(placeholder(i - 1));
            pieces.push(r);
        });
        this.template.innerHTML = pieces.join('');
        this.indexPlaceholders();
    }

    private indexTextNode(n: Text, path: number[]): ChildNode | null {
        const { constantParts, placeholders } =
            splitByPlaceholders(n.textContent ?? '');
        constantParts.forEach((c, i) => {
            if (i > 0) {
                n.parentNode?.insertBefore(makePlaceholder(), n);
            }
            n.parentNode?.insertBefore(document.createTextNode(c), n);
        });
        const nextN = n.nextSibling;
        n.parentNode?.removeChild(n);
        placeholders.forEach((n, i) => {
            const currentPath = path.slice();
            currentPath[currentPath.length - 1] += i * 2 + 1;
            this.placeholderActions.push({
                path: currentPath,
                placeholder: new NodeInserter(n),
            });
        });
        path[path.length - 1] += constantParts.length + placeholders.length;
        return nextN;
    }

    private indexElement(e: Element, path: number[]) {
        const vars: VariablePlaceholder[] = [];
        for (let i = 0; i < e.attributes.length; i++) {
            const attr = e.attributes[i];
            const attrName = attr.name;
            const nameIsPlaceholder = attrName.match(onePlaceholderRe);
            if (nameIsPlaceholder !== null) {
                e.removeAttributeNode(attr);
                i--;
                const n = parseInt(nameIsPlaceholder[1], 10);
                vars.push(new AttributesInserter(n));
            } else {
                const { constantParts, placeholders } =
                    splitByPlaceholders(attr.value);
                if (constantParts.length !== 1) {
                    vars.push(new AttributeValueInserter({attrName, constantParts, placeholders}));
                }
            }
        }
        if (vars.length) {
            this.placeholderActions.push({
                path: path.slice(),
                placeholder: vars.length === 1 ? vars[0] : new CompoundPlaceholder(vars),
            });
        }
    }

    private indexPlaceholders() {
        const path: number[] = [];
        const walk = (parentNode: ParentNode) => {
            path.push(0);
            let nextN = parentNode.firstChild;
            while (nextN !== null) {
                const n = nextN;
                switch (n.nodeType) {
                    case Node.TEXT_NODE:
                        nextN = this.indexTextNode(n as Text, path);
                        break;
                    case Node.ELEMENT_NODE: {
                        const e = n as Element;
                        this.indexElement(e, path)
                        walk(e);
                        nextN = e.nextSibling;
                        path[path.length - 1]++;
                        break;
                    }
                    default:
                        nextN = n.nextSibling;
                        path[path.length - 1]++;
                        break;
                }
            }
            path.pop();
        };
        walk(this.template.content);
    }

    clone(): HtmlFragmentInstance {
        const container = this.template.content.cloneNode(true) as ParentNode;
        const locations = this.placeholderActions.map(({ path, placeholder }) =>
            placeholder.install(followPath(container, path)));
        return new HtmlFragmentInstance(container, locations);
    }
}

// Nifty trick: TemplateStringsArray instances are interned so it makes sense to key a cache
// based on their object identity!
const templateCache = new WeakMap<TemplateStringsArray, HtmlFragmentBuilder>();

export type HtmlTemplater =
    (constantParts: TemplateStringsArray, ... variableParts: Array<HtmlFragment>)
    => ChildNode[];

export function template(): HtmlTemplater {
    const instanceCache = new WeakMap<TemplateStringsArray, HtmlFragmentInstance>();
    return (constantParts, ... variableParts) => {
        let b = templateCache.get(constantParts);
        if (b === void 0) {
            b = new HtmlFragmentBuilder(constantParts);
            templateCache.set(constantParts, b);
        }
        let i = instanceCache.get(constantParts);
        if (i === void 0) {
            i = b.clone();
            instanceCache.set(constantParts, i);
        }
        return i.update(variableParts);
    };
}
