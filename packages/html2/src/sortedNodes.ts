/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { AnyValue, stringify, parse, is, compare } from '@syndicate-lang/core';

export const SYNDICATE_SORT_KEY = '__syndicate_sort_key';

export function setSortKey(n: HTMLOrSVGElement | Node, key: AnyValue | null) {
    if (key === null) {
        if ('dataset' in n) {
            // html element nodes etc.
            delete n.dataset[SYNDICATE_SORT_KEY];
        } else {
            // text nodes, svg nodes, etc etc.
            delete (n as any)[SYNDICATE_SORT_KEY];
        }
    } else {
        const v = stringify(key);
        if ('dataset' in n) {
            // html element nodes etc.
            n.dataset[SYNDICATE_SORT_KEY] = v;
        } else {
            // text nodes, svg nodes, etc etc.
            (n as any)[SYNDICATE_SORT_KEY] = v;
        }
    }
}

export function getSortKey(n: HTMLOrSVGElement | Node): AnyValue | null {
    if ('dataset' in n && n.dataset[SYNDICATE_SORT_KEY]) {
        return parse(n.dataset[SYNDICATE_SORT_KEY]!);
    }
    if ((n as any)[SYNDICATE_SORT_KEY]) {
        return parse((n as any)[SYNDICATE_SORT_KEY]);
    }
    return null;
}

export function hasSortKey(n: HTMLOrSVGElement | Node, key: AnyValue): boolean {
    let v = getSortKey(n);
    return (v !== null) && is(v, key);
}

function firstChildNodeIndex_withSortKey(n: Node): number {
    for (let i = 0; i < n.childNodes.length; i++) {
        if (getSortKey(n.childNodes[i]) !== null) return i;
    }
    return n.childNodes.length;
}

// If *no* nodes have a sort key, returns a value that yields an empty
// range in conjunction with firstChildNodeIndex_withSortKey.
function lastChildNodeIndex_withSortKey(n: Node): number {
    for (let i = n.childNodes.length - 1; i >= 0; i--) {
        if (getSortKey(n.childNodes[i]) !== null) return i;
    }
    return n.childNodes.length - 1;
}

function isGreaterThan(a: AnyValue, b: AnyValue): boolean {
    return compare(a, b) > 0;
}

export function findInsertionPoint(n: Node, key: AnyValue): ChildNode | null {
    let lo = firstChildNodeIndex_withSortKey(n);
    let hi = lastChildNodeIndex_withSortKey(n) + 1;
    // lo <= hi, and [lo, hi) have sort keys.

    while (lo < hi) { // when lo === hi, there's nothing more to examine.
        let probe = (lo + hi) >> 1;
        let probeSortKey = getSortKey(n.childNodes[probe])!;

        if (isGreaterThan(probeSortKey, key)) {
            hi = probe;
        } else {
            lo = probe + 1;
        }
    }

    // lo === hi now.
    if (lo < n.childNodes.length) {
        return n.childNodes[lo];
    } else {
        return null;
    }
}
