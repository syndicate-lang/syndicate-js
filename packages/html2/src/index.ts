/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { AnyValue, Turn, Facet, Dataflow, Dataspace, Ref } from "@syndicate-lang/core";
import { getSortKey, setSortKey, findInsertionPoint } from './sortedNodes';
import { HtmlTemplater, template } from "./html";
export { HtmlTemplater, template, HtmlFragment } from "./html";
export * as Html from './html';
export * as SortedNodes from './sortedNodes';

export assertion type LocationHash(hash: string);
export type LocationHash = ReturnType<typeof LocationHash>;

export function boot(ds = Dataspace.local) {
    spawnLocationHashTracker(ds);
}

type Wrapped = {
    wrapped: EventListenerOrEventListenerObject,
    options?: AddEventListenerOptions | boolean,
};

export type NodeGenerator = (t: HtmlTemplater) => ReturnType<HtmlTemplater>;

function isStringOrTemplate(x: any): x is string | HTMLTemplateElement {
    return typeof x === 'string' || (typeof x === 'object' && x instanceof HTMLTemplateElement);
}

export type WidgetOptions = {
    eventSelector?: string | undefined;
};

export class Widget implements EventTarget {
    readonly facet: Facet;
    readonly widgetOptions: WidgetOptions;
    private _node: ChildNode | null = null;
    private _eventNode: ChildNode | null = null;
    callbacks = new Map<string, Map<EventListenerOrEventListenerObject, Wrapped>>();

    get node(): ChildNode {
        return this._node!;
    }

    get eventNode(): ChildNode {
        return this._eventNode!;
    }

    constructor (node: ChildNode, options?: WidgetOptions);
    constructor (nodeGenerator: NodeGenerator, options?: WidgetOptions);
    constructor (template: string | HTMLTemplateElement, data: object, options?: WidgetOptions);
    constructor (arg0: ChildNode | NodeGenerator | string | HTMLTemplateElement, arg1?: object | WidgetOptions, arg2?: WidgetOptions) {
        let nodeGenerator: NodeGenerator;

        let maybeOptions: WidgetOptions | undefined;
        if (isStringOrTemplate(arg0)) {
            nodeGenerator = templateGenerator(arg0, arg1 as object);
            maybeOptions = arg2;
        } else if (typeof arg0 === 'function') {
            nodeGenerator = arg0 as NodeGenerator;
            maybeOptions = arg1;
        } else {
            nodeGenerator = () => [arg0];
            maybeOptions = arg1;
        }
        this.widgetOptions = Object.assign({
            eventSelector: void 0,
        }, maybeOptions ?? {});

        this.facet = Turn.activeFacet;

        const cancelAtExit = this.facet.actor.atExit(() => this.node.remove());
        on stop {
            this.node.remove();
            cancelAtExit();
        }

        const thisTemplate = template();
        dataflow {
            const nodes = nodeGenerator(thisTemplate);
            if (nodes.length !== 1) {
                throw new Error(`@syndicate-lang/html2: Expected exactly one node from template`);
            }
            if (this._node === null) {
                this._node = nodes[0];
            } else if (this._node !== nodes[0]) {
                throw new Error(`@syndicate-lang/html2: Node generator is not stable`);
            }
            this._eventNode = this.widgetOptions.eventSelector === void 0
                ? this._node
                : (this._nodeAsParent?.querySelector(this.widgetOptions.eventSelector) ?? null);
            if (this._eventNode === null) {
                throw new Error(`@syndicate-lang/html2: Missing event node`);
            }
        }
    }

    get _nodeAsParent(): ParentNode | null {
        if (this._node && 'querySelector' in this._node) {
            return this._node as unknown as ParentNode;
        } else {
            return null;
        }
    }

    get parent(): ParentNode | null {
        return this.node.parentNode;
    }

    set parent(p: string | ParentNode | null) {
        this.setParent(p);
    }

    get sortKey(): AnyValue | null {
        return getSortKey(this.node);
    }

    set sortKey(k: AnyValue | null) {
        setSortKey(this.node, k);
        const p = this.parent;
        if (p !== null) {
            this.setParent(null);
            this.setParent(p);
        }
    }

    setSortKey(k: AnyValue | null): this {
        this.sortKey = k;
        return this;
    }

    setParent(p: string | ParentNode | null, wrt: ParentNode = document): this {
        if (typeof p === 'string') {
            p = wrt.querySelector(p);
        }

        if (this.node.parentNode !== p) {
            if (p === null) {
                this.node.remove();
            } else {
                const k = this.sortKey;
                if (k !== null) {
                    const sib = findInsertionPoint(p, k);
                    p.insertBefore(this.node, sib);
                } else {
                    p.appendChild(this.node);
                }
            }
        }

        return this;
    }

    querySelector(selector: string): Widget | null;
    querySelector<T extends Widget>(selector: string, ctor: { new(e: Element): T }): T | null;
    querySelector<T extends Widget>(selector: string, ctor?: { new(e: Element): T }): Widget | null {
        const e = this._nodeAsParent?.querySelector(selector);
        return e ? new (ctor ?? Widget)(e) : null;
    }

    querySelectorAll(selector: string): Widget[];
    querySelectorAll<T extends Widget>(selector: string, ctor: { new(e: Element): T }): T[];
    querySelectorAll<T extends Widget>(selector: string, ctor?: { new(e: Element): T }): Widget[] {
        const es = this._nodeAsParent?.querySelectorAll(selector);
        const ws: Widget[] = [];
        if (es) es.forEach(e => ws.push(new (ctor ?? Widget)(e)));
        return ws;
    }

    on(type: string, callback: EventListenerOrEventListenerObject): this {
        this.addEventListener(type, callback);
        return this;
    }

    once(type: string, callback: EventListenerOrEventListenerObject): this {
        this.addEventListener(type, callback, { once: true });
        return this;
    }

    off(type: string, callback: EventListenerOrEventListenerObject): this {
        this.removeEventListener(type, callback);
        return this;
    }

    addEventListener(
        type: string,
        callback: EventListenerOrEventListenerObject | null,
        options?: AddEventListenerOptions | boolean,
    ): void {
        if (callback === null) return;

        let cbs = this.callbacks.get(type);
        if (cbs === void 0) {
            cbs = new Map();
            this.callbacks.set(type, cbs);
        } else {
            if (cbs.has(callback)) return;
        }

        const entry: Wrapped = {
            wrapped: (typeof callback === 'function')
                ? evt => this.facet.turn(() => callback(evt))
                : evt => this.facet.turn(() => callback.handleEvent(evt)),
            options,
        };
        cbs.set(callback, entry);

        this.eventNode.addEventListener(type, entry.wrapped, options);
    }

    dispatchEvent(event: Event): boolean {
        return this.eventNode.dispatchEvent(event);
    }

    removeEventListener(
        type: string,
        callback: EventListenerOrEventListenerObject | null,
        options?: boolean | EventListenerOptions | undefined,
    ): void {
        if (callback === null) return;

        const cbs = this.callbacks.get(type);
        if (cbs === void 0) return;

        const r = cbs.get(callback);
        if (r === void 0) return;

        this.eventNode.removeEventListener(type, r.wrapped, options);

        cbs.delete(callback);
        if (cbs.size === 0) this.callbacks.delete(type);
    }
}

export type ValueWidgetOptions = {
    triggerEvent?: 'change' | 'input' | undefined,
} & WidgetOptions;

export class ValueWidget extends Widget {
    readonly valueWidgetOptions: ValueWidgetOptions;
    _value: Dataflow.Field<string>;
    _valueAsNumber: Dataflow.Field<number>;
    _checked: Dataflow.Field<boolean>;

    constructor (node: ChildNode, options?: ValueWidgetOptions);
    constructor (nodeGenerator: NodeGenerator, options?: ValueWidgetOptions);
    constructor (template: string | HTMLTemplateElement, data: object, options?: ValueWidgetOptions);
    constructor (arg0: ChildNode | NodeGenerator | string | HTMLTemplateElement, arg1?: object | ValueWidgetOptions, arg2?: ValueWidgetOptions) {
        super(arg0 as any, arg1 as any, arg2 as any);

        this.valueWidgetOptions = Object.assign({
            triggerEvent: void 0,
        } as ValueWidgetOptions, this.widgetOptions);

        field value: string = '';
        this._value = value;

        field valueAsNumber: number = NaN;
        this._valueAsNumber = valueAsNumber;

        field checked: boolean = false;
        this._checked = checked;

        const hasValue = 'value' in this.eventNode;
        const hasChecked = 'checked' in this.eventNode;
        if (hasValue || hasChecked) {
            this.on(this.valueWidgetOptions.triggerEvent ?? 'change', () => this.readValues());
            this.readValues();

            if (hasValue) {
                dataflow { this.valueAsNumber = this._valueAsNumber.value; }
                dataflow { this.value = this._value.value; }
            }
            if (hasChecked) {
                dataflow { this.checked = this._checked.value; }
            }
        }
    }

    readValues() {
        const n = this.eventNode as any;
        this.suppressCycleWarning();
        this._value.value = n.value ?? '';
        this._valueAsNumber.value = n.valueAsNumber ?? NaN;
        this._checked.value = n.checked ?? false;
    }

    get value(): string {
        return this._value.value;
    }

    set value(v: string) {
        (this.eventNode as any).value = v;
        this._value.value = v;
    }

    get valueAsNumber(): number {
        return this._valueAsNumber.value;
    }

    set valueAsNumber(v: number) {
        (this.eventNode as any).value = Number.isNaN(v) ? '' : '' + v;
        this._valueAsNumber.value = v;
    }

    get checked(): boolean {
        return this._checked.value;
    }

    set checked(v: boolean) {
        (this.eventNode as any).checked = v;
        this._checked.value = v;
    }

    suppressCycleWarning(): void {
        this._value.suppressCycleWarning();
        this._valueAsNumber.suppressCycleWarning();
        this._checked.suppressCycleWarning();
    }
}

function spawnLocationHashTracker(ds: Ref) {
    spawn named 'LocationHashTracker' {
        at ds {
            field hashValue: string = '/';

            const loadHash = () => {
                var h = decodeURIComponent(window.location.hash);
                if (h.length && h[0] === '#') {
                    h = h.slice(1);
                }
                hashValue.value = h || '/';
            };
            const facet = Turn.activeFacet;
            const handlerClosure = () => facet.turn(loadHash);

            window.addEventListener('hashchange', handlerClosure);
            on stop window.removeEventListener('hashchange', handlerClosure);

            loadHash();
            assert LocationHash(hashValue.value);

            on message LocationHash($newHash: string) => {
                window.location.hash = newHash;
            }
        }
    }
}

export function templateGenerator(
    template0: string | HTMLTemplateElement,
    data: object,
): NodeGenerator {
    const template = typeof template0 === 'string' ? document.querySelector(template0) : template0;
    if (template === null) throw new Error('Cannot find template: ' + template0);
    const body = `return t => t\`${template.innerHTML.trim().split('`').join('\\`')}\``;
    const kvs = Object.entries(data);
    const keys = kvs.map(e => e[0]);
    const values = kvs.map(e => e[1]);
    const factory = new Function(... keys, body);
    return factory(... values);
}
