/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { HtmlTemplater, template } from '../src/html';
import { compareHTML } from './test-utils';

describe('basic templating', () => {
    it('should produce a node', () => {
        const x = document.createElement('x');
        x.appendChild(document.createTextNode('y'));
        expect(template()`<x>y</x>`).toEqual([x]);
    });

    it('should substitute a string', () => {
        const y = 'abc';
        compareHTML(template()`<x>${y}</x>`, '<x>abc</x>');
    });

    it('should substitute a string without surrounding context', () => {
        const y = 'abc';
        compareHTML(template()`${y}`, 'abc');
    });

    it('should substitute multiple strings', () => {
        const y = 'abc';
        compareHTML(template()`<x>${y} ${y} ${y}</x>`, '<x>abc abc abc</x>');
    });

    it('should substitute a node', () => {
        const y = template()`<z>q</z>`;
        compareHTML(template()`<x>${y}</x>`, '<x><z>q</z></x>');
    });

    it('should substitute an array of strings', () => {
        const y = ['abc', 'def'];
        compareHTML(template()`<x>${y}</x>`, '<x>abcdef</x>');
    });

    it('should substitute an array of strings and nodes', () => {
        const y = ['abc', template()`<z>q</z>`, 'def'];
        compareHTML(template()`<x>${y}</x>`, '<x>abc<z>q</z>def</x>');
    });

    it('should substitute into attributes', () => {
        const v = () => 'aaa';
        const ps = [() => '123', () => '234'];
        compareHTML(template()`<q><x t="${v()}">${ps.map(p => p())}</x></q>`,
                    '<q><x t="aaa">123234</x></q>');
    });

    it('should substitute into attributes, with children', () => {
        const v = () => 'aaa';
        const ps = [() => '123', () => '234'];
        const f = 'z';
        compareHTML(template()`<q><x t="${v()}">${f}</x>${ps.map(p => p())}</q>`,
                    '<q><x t="aaa">z</x>123234</q>');
    });

    it('should substitute into attributes, with children and whitespace', () => {
        const v = () => 'aaa';
        const ps = [() => '123', () => '234'];
        const f = 'z';
        compareHTML(template()`<q><x ttt="${v()}">${f}</x>
            ${ps.map(p => p())}
          </q>`,
                    `<q><x ttt="aaa">z</x>
            123234
          </q>`);
    });

    it('example from paradise.js', () => {
        const pieces = ['C', 'D'];
        compareHTML(template()`<p class="event">
        <span class="timestamp" title="${'A'}">${'B'}</span>
        ${pieces.map(p => p)}
      </p>`,
                    `<p class="event">
        <span class="timestamp" title="A">B</span>
        CD
      </p>`);
    });

    it('should be callable multiple times', () => {
        const v = () => 'aaa';
        const ps = [() => '123', () => '234', () => '345'];
        const expected = '<q><x t="aaa">123234345</x></q>';
        const t = template();
        compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, expected);
        compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, expected);
        compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, expected);
    });

    it('should be callable multiple times with the same syntactic location', () => {
        // Because the fixed portion of a template string, the TemplateStringsArray, is hashconsed
        const v = () => 'aaa';
        const ps = [() => '123', () => '234', () => '345'];
        const expected = '<q><x t="aaa">123234345</x></q>';
        const t = template();
        const f = () => compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, expected);
        f();
        f();
        f();
    });

    it('should be callable multiple times with extra items', () => {
        const v = () => 'aaa';
        const t = template();
        const ps = [() => '1'];
        compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, '<q><x t="aaa">1</x></q>');
        ps.push(() => '2');
        compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, '<q><x t="aaa">12</x></q>');
        ps.push(() => '3');
        compareHTML(t`<q><x t="${v()}">${ps.map(p => p())}</x></q>`, '<q><x t="aaa">123</x></q>');
    });

    it('should be reusable for supplying arguments to itself', () => {
        const inner = (t: HtmlTemplater) => t`<i>middle</i>`;
        const t = template();
        compareHTML(t`<p>n${inner(t)}n</p>`, '<p>n<i>middle</i>n</p>');
    });

    it('should accept strings', () => {
        const inner = (_t: HtmlTemplater) => `<i>middle</i>`;
        const t = template();
        compareHTML(t`<p>n${inner(t)}n</p>`, '<p>n&lt;i&gt;middle&lt;/i&gt;n</p>');
    });
});

describe('template output node stability', () => {
    it('should work for simple use of a template', () => {
        const t = template();
        let i = 0;
        const p = () => t`<i>${i++}</i>`;
        const n1 = p();
        compareHTML(n1, '<i>0</i>');
        const n2 = p();
        compareHTML(n2, '<i>1</i>');
        expect(n1[0]).toBe(n2[0]);
    });

    it('should work for reuse of a template', () => {
        const t = template();
        let i = 0;
        const q = () => t`<b>${i++}</b>`;
        const p = () => t`<i>${q()}</i>`;
        const n1 = p();
        compareHTML(n1, '<i><b>0</b></i>');
        const n2 = p();
        compareHTML(n2, '<i><b>1</b></i>');
        expect(n1[0]).toBe(n2[0]);
    });
});

describe('handling of externally-added nodes', () => {
    it('should be updatable with node added in front', () => {
        const t = template();
        let v: string | number = 2;
        const f = () => t`<p><span>one</span><span>${v}</span><span>three</span></p>`;
        const n = document.createElement('span');
        n.innerHTML = 'zero';
        let a = f();
        a[0].insertBefore(n, a[0].childNodes[0]);
        compareHTML(a, `<p><span>zero</span><span>one</span><span>2</span><span>three</span></p>`);
        v = 'two';
        a = f();
        a[0].insertBefore(n, a[0].childNodes[0]);
        compareHTML(a, `<p><span>zero</span><span>one</span><span>two</span><span>three</span></p>`);
    });
});

describe('textareas', () => {
    it('should update', () => {
        const t = template();
        let value = 'one';
        const f = () => t`<textarea>${value}</textarea>`;
        compareHTML(f(), `<textarea>one</textarea>`);
        value = 'two';
        compareHTML(f(), `<textarea>two</textarea>`);
    });
});
