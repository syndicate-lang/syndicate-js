/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

// https://github.com/jsdom/jsdom/issues/2524

import { TextEncoder, TextDecoder } from 'util';
import $JSDOMEnvironment from 'jest-environment-jsdom';
export default class JSDOMEnvironment extends $JSDOMEnvironment {
  constructor(... args) {
    const { global } = super(... args);
    if (!global.TextEncoder) global.TextEncoder = TextEncoder;
    if (!global.TextDecoder) global.TextDecoder = TextDecoder;
  }
}
