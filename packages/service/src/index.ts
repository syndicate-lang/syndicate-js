/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Actor, AnyValue, Relay, Turn, assertionFacetObserver } from "@syndicate-lang/core";
import * as process from 'process';

export interface ServiceOptions {
    redirectConsole?: boolean;
    debug?: boolean;
    trustPeer?: boolean;
}

export function service(handler: (args: AnyValue) => void, options?: ServiceOptions): Actor {
    if (options?.redirectConsole ?? true) {
        console.info = console.log = console.warn;
    }
    return Actor.boot(() => {
        const facet = Turn.activeFacet;
        facet.preventInertCheck();
        facet.actor.atExit(() => process.exit((facet.actor.exitReason?.ok ?? false) ? 0 : 1));
        new Relay.Relay(Object.assign({
            packetWriter: (bs: Uint8Array) => process.stdout.write(bs),
            setup(r: Relay.Relay) {
                process.stdin.on('data', bs => facet.turn(() => r.accept(new Uint8Array(bs))));
                process.stdin.on('close', () => facet.turn(() => { stop facet; }));
                process.stdin.on('end', () => facet.turn(() => { stop facet; }));
            },
            initialRef: Turn.ref(assertionFacetObserver(handler)),
        }, options ?? {}));
    });
}
