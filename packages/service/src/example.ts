/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { stringify } from '@syndicate-lang/core';
import { service } from './index.js';

service(args => {
    console.log('+', stringify(args));
    on stop { console.log('-', stringify(args)); }
});
