# esbuild plugin for Syndicate/JS

```javascript
import esbuild from 'esbuild';
import { syndicatePlugin } from '@syndicate-lang/esbuild-plugin';

...

esbuild.build({
  ...
  plugins: [..., syndicatePlugin(), ...],
  ...
});

...
```

The `syndicatePlugin` function takes an optional options object. It may include properties:

 - `module`, optional, drawn from `"es6"` (the default), `"require"`, `"global"`, or `"none"`:
   controls how to get hold of an instance of the `@syndicate-lang/core` module. If `es6`, uses
   `import`; if `require`, uses `require`; if `global`, uses a global variable (useful for
   browser contexts); if `none`, does no imports, so some other provision will have to be made
   to ensure `__SYNDICATE__` is bound to a module instance for the output code.

 - `runtime`, optional `string`: defaults to `"@syndicate-lang/core"`. For `module` of `es6` or
   `require`, names the module to import/require, respectively; for `module` of `global`, an
   expression to yield a module instance.

   For example, in a browser, using a CDN to retrieve a `dist/syndicate.js` script from the
   `@syndicate-lang/core` package produces a `Syndicate` global variable, so you could set
   `module` to `global` and `runtime` to `Syndicate` or `window.Syndicate` etc.

 - `include`, `exclude`: both optional arrays of strings. By default, `include` is
   `["**/*.{js,ts}"]` and `exclude` is `["node_modules/**"]`. Each time esbuild considers a
   module path, the Syndicate plugin first checks to see if it matches some glob in `include`
   (using [outmatch](https://www.npmjs.com/package/outmatch)). If not, the plugin delegates to
   esbuild's default handler. Otherwise, it checks to see if it matches some glob in `exclude`.
   If so, again it delegates to esbuild. Otherwise, it runs the Syndicate compiler on the
   module source code and hands that off to esbuild.
