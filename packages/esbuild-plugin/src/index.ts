/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import * as esbuild from 'esbuild';
import * as C from '@syndicate-lang/compiler';
import outmatch from 'outmatch';
import path from 'node:path';
import fs from 'node:fs';

import Pos = C.Syntax.Pos;

export interface SyndicatePluginOptions {
    runtime?: string,
    module?: C.Codegen.ModuleType,
    include?: Array<string>, // glob patterns
    exclude?: Array<string>, // glob patterns
}

class Errors {
    errors: Array<{ message: string, start: Pos | undefined, end: Pos | undefined }> = [];

    push(message: string, start: Pos | undefined, end: Pos | undefined) {
        this.errors.push({ message, start, end });
    }

    toEsbuild(): Array<esbuild.PartialMessage> {
        return this.errors.map(e => {
            const p = e.start ?? e.end ?? null;
            const location = p && {
                file: p.name ?? '<unknown>',
                line: p.line,
                column: p.column,
            };
            return {
                text: e.message,
                location,
            };
        });
    }
}

export function syndicatePlugin(options?: SyndicatePluginOptions): esbuild.Plugin {
    const includes = (options?.include ?? ['**/*.{js,ts}']).map(i => outmatch(i));
    const excludes = (options?.exclude ?? ['node_modules/**']).map(i => outmatch(i));

    return {
        name: 'syndicate',
        setup(build) {
            build.onLoad({ filter: /\.(js|ts)$/ }, async (args) => {
                const name = path.relative(process.cwd(), args.path);
                if (!includes.find(i => i(name))) return null;
                if (excludes.find(o => o(name))) return null;

                const typescript = name.endsWith('ts');
                const source = await fs.promises.readFile(args.path, 'utf8');
                const errors = new Errors();

                const { text, map } = C.compile({
                    source,
                    name: path.basename(args.path),
                    runtime: options?.runtime,
                    module: options?.module,
                    typescript,
                    emitError: (message, start, end) => errors.push(message, start, end),
                });

                const mapBase64 = Buffer.from(JSON.stringify(map)).toString('base64');
                const contents = text +
                    '\n//# sourceMappingURL=data:application/json;base64,' + mapBase64;
                return { contents, warnings: errors.toEsbuild(), loader: typescript ? 'ts' : 'js' };
            });
        }
    };
}
