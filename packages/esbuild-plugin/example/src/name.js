/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Dataspace } from '@syndicate-lang/core';

assertion type Name(n);

spawn {
  at Dataspace.local {
    assert Name('a Syndicate actor');
  }
}
