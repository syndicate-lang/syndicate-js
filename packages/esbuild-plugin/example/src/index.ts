/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Dataspace } from '@syndicate-lang/core';
import './name.js';

assertion type Name(n: string);

spawn {
    at Dataspace.local {
        during Name($n) => {
            console.log(`hello from ${n}`);
        }
    }
}
