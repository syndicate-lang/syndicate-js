// -*- javascript -*-, obviously, Emacs
import esbuild from 'esbuild';
import { syndicatePlugin } from '@syndicate-lang/esbuild-plugin';

const mode = process.argv[2] ?? 'help';

if (mode !== 'build' && mode !== 'watch') {
  console.error('Usage: node esbuild.mjs <build | watch>');
  process.exit(1);
}

const config = {
  entryPoints: ["src/index.ts"],
  bundle: true,
  format: 'esm',
  outfile: 'index.js',
  platform: 'node',
  sourcemap: true,
  plugins: [syndicatePlugin()],
  logLevel: 'info',
};

switch (mode) {
  case 'watch': {
    const ctx = await esbuild.context(config);
    await ctx.watch();
    break;
  }
  case 'build': {
    esbuild.build(config);
    break;
  }
}
