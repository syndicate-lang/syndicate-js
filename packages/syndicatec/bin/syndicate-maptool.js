#!/usr/bin/env node
/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

try {
  require('../lib/maptool.js').main(process.argv.slice(2));
} catch (e) {
  console.error(e);
  process.exit(1);
}
