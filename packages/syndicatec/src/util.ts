/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

export function dataURL(s: string): string {
    return `data:application/json;base64,${Buffer.from(s).toString('base64')}`;
}

export function sourceMappingComment(url: string): string {
    return `\n//# sourceMappingURL=${url}`;
}
