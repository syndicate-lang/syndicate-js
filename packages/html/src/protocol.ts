/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

// Message. Interest in this causes event listeners to be added for
// the given eventType to all nodes matching the given selector *at
// the time of the subscription*. As nodes *from this library* come
// and go, they will have event handlers installed and removed as
// well.
//
// WARNING: The current simple implementation re-scans the whole
// document anytime a change is signalled; in future, it may not do
// such a scan.
//
export message type GlobalEvent(selector, eventType, event);

// Message. As GlobalEvent, but instead of using a selector to choose
// target DOM nodes, attaches an event handler to the browser "window"
// object itself.
//
export message type WindowEvent(eventType, event);

// Message. Like GlobalEvent, but applies only within the scope of the
// UI fragment identified.
//
export message type UIEvent(fragmentId, selector, eventType, event);

// Assertion. Causes the setup of DOM nodes corresponding to the given
// HTML fragment, as immediate children of all nodes named by the
// given selector that exist at the time of assertion. The orderBy
// field should be null, a string, or a number. Fragments are ordered
// primarily by orderBy, and secondarily by fragmentId.
//
export assertion type UIFragment(fragmentId, selector, html, orderBy);

// Assertion. Asserted by respondent to a given UIFragment.
//
export assertion type UIFragmentVersion(fragmentId, version);

// Assertion. Causes the setup of DOM attributes on all nodes named by
// the given selector that exist at the time of assertion.
//
// NOTE: Attribute "class" is a special case: it treats the value of
// the attribute as a (string encoding of a) set. The given value is
// split on whitespace, and each piece is added to the set of things
// already present. (See the implementation for details.)
//
export assertion type UIAttribute(selector, attribute, value);

// Assertion. Similar to UIAttribute, but for properties of DOM nodes.
//
export assertion type UIProperty(selector, property, value);

// Assertion. For clients to monitor the values of properties that,
// when changed, emit 'change' events.
//
export assertion type UIChangeableProperty(selector, property, value);

// Messages.
// NOTE: These do not treat "class" specially!
//
export message type SetAttribute(selector, attribute, value);
export message type RemoveAttribute(selector, attribute);
export message type SetProperty(selector, property, value);
export message type RemoveProperty(selector, property);

// Assertion. Current "location hash" -- the "#/path/part" fragment at
// the end of window.location.
//
export assertion type LocationHash(value);

// Message. Causes window.location to be updated to have the given new
// "location hash" value.
//
export message type SetLocationHash(value);
