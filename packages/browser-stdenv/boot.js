/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

const SyndicateBrowserStdenv = {
  autoboot: true,
  autoboot_options: {
    SyndicateWsRelay: {
      debug: false,
    },
  },
  boot() {
    SyndicateHtml2.boot();
    SyndicateWsRelay.boot(void 0, this.autoboot_options.SyndicateWsRelay.debug);
  }
};

window.addEventListener('DOMContentLoaded', () => {
  if (SyndicateBrowserStdenv.autoboot) {
    SyndicateBrowserStdenv.boot();
  }
});
