#!/bin/sh

set -e

cd "$(dirname "$0")"

M=../../node_modules

cat \
    $M/@preserves/core/dist/preserves.js \
    $M/@preserves/schema/dist/preserves-schema.js \
    $M/@preserves/schema/dist/preserves-schema-browser.js \
    $M/@syndicate-lang/core/dist/syndicate.js \
    $M/@syndicate-lang/html2/dist/syndicate-html2.js \
    $M/@syndicate-lang/ws-relay/dist/syndicate-ws-relay.js \
    $M/@syndicate-lang/compiler/dist/syndicate-compiler.js \
    $M/@syndicate-lang/compiler/dist/syndicate-browser-compiler.js \
    boot.js \
    > index.js

cp \
    $M/@syndicate-lang/core/dist/syndicate.js.map \
    $M/@syndicate-lang/html2/dist/syndicate-html2.js.map \
    $M/@syndicate-lang/ws-relay/dist/syndicate-ws-relay.js.map \
    $M/@syndicate-lang/compiler/dist/syndicate-compiler.js.map \
    .

cat \
    $M/@preserves/core/dist/preserves.min.js \
    $M/@preserves/schema/dist/preserves-schema.min.js \
    $M/@preserves/schema/dist/preserves-schema-browser.js \
    $M/@syndicate-lang/core/dist/syndicate.min.js \
    $M/@syndicate-lang/html2/dist/syndicate-html2.min.js \
    $M/@syndicate-lang/ws-relay/dist/syndicate-ws-relay.min.js \
    $M/@syndicate-lang/compiler/dist/syndicate-compiler.min.js \
    $M/@syndicate-lang/compiler/dist/syndicate-browser-compiler.js \
    boot.js \
    > index.min.js

cp \
    $M/@syndicate-lang/core/dist/syndicate.min.js.map \
    $M/@syndicate-lang/html2/dist/syndicate-html2.min.js.map \
    $M/@syndicate-lang/ws-relay/dist/syndicate-ws-relay.min.js.map \
    $M/@syndicate-lang/compiler/dist/syndicate-compiler.min.js.map \
    .

echo $(date) $(pwd) 'build complete.'
