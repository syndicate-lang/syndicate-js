/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Bytes, underlying } from '@preserves/core';
import { makeHMAC, BLAKE2s, randomBytes } from 'salty-crypto';

export const KEY_LENGTH = 16; // 128 bits

export async function newKey(): Promise<Bytes> {
    return Bytes.from(randomBytes(KEY_LENGTH));
}

const HMAC_BLAKE2s = makeHMAC(BLAKE2s);

export function mac(secretKey: Bytes, data: Bytes): Bytes {
    return Bytes.from(HMAC_BLAKE2s(underlying(secretKey), underlying(data))
        .subarray(0, KEY_LENGTH));
}
