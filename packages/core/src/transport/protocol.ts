/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import * as S from '../gen/sturdy.js';
import { Decoder, DecoderState, Encoder, EncoderState, neverEmbeddedType, EmbeddedType, Value, Embedded, EmbeddedWriter } from '@preserves/core';

export const wireRefEmbeddedType: EmbeddedType<Embedded<S.WireRef>> & EmbeddedWriter<Embedded<S.WireRef>> = {
    decode(s: DecoderState): Embedded<S.WireRef> {
        return new Embedded<S.WireRef>(S.asWireRef(new Decoder<any>(s).next()));
    },

    encode(s: EncoderState, v: Embedded<S.WireRef>): void {
        new Encoder<any>(s, neverEmbeddedType).push(S.fromWireRef(v.value));
    },

    fromValue(v: Value): Embedded<S.WireRef> {
        return new Embedded<S.WireRef>(S.asWireRef(v as Value<S._embedded>));
    },

    toValue(v: Embedded<S.WireRef>): Value {
        return S.fromWireRef(v.value) as Value;
    }
};
