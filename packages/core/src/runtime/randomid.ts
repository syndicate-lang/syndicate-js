/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Bytes } from '@preserves/core';
import { randomBytes } from 'salty-crypto';

export function _btoa(s: string): string {
    try {
        return btoa(s);
    } catch (e) {
        return Buffer.from(s).toString('base64');
    }
}

export function randomId(byteCount: number, hexOutput: boolean = false): string {
    let buf = randomBytes(byteCount);
    if (hexOutput) {
        return Bytes.from(buf).toHex();
    } else {
        return _btoa(String.fromCharCode.apply(null, buf as unknown as number[])).replace(/=/g,'');
    }
}
