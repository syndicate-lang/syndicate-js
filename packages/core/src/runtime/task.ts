/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import type { Facet } from './actor.js';

import * as q from '../gen/queuedTasks.js';

export type Task = StructuredTask<TaskDescription>;

export interface StructuredTask<T> {
    perform(): void;
    describe(): T;
}

export type TaskDescription =
    | { type: 'bootActor', detail: q.OptionalAny }
    | { type: 'turn', tasks: TaskAction[] }
    | { type: 'externalTurn' }
;

export type TaskAction = { targetFacet: Facet, action: ActionDescription };
export type ActionDescription = q.ActionDescription;
