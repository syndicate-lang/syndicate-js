/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

export type NonEmptyStack<T> = { item: T, rest: Stack<T> };
export type Stack<T> = null | NonEmptyStack<T>;

export function empty<T>(): Stack<T> {
    return null;
}

export function push<T>(item: T, rest: Stack<T>): NonEmptyStack<T> {
    return { item, rest };
}

export function isEmpty<T>(s: Stack<T>): s is null {
    return s === null;
}

export function nonEmpty<T>(s: Stack<T>): s is NonEmptyStack<T> {
    return s !== null;
}

export function rest<T>(s: Stack<T>): Stack<T> {
    if (nonEmpty(s)) {
        return s.rest;
    } else {
        throw new Error("pop from empty Stack");
    }
}

export function drop<T>(s: Stack<T>, n: number): Stack<T> {
    while (n--) s = rest(s);
    return s;
}

export function dropNonEmpty<T>(s: NonEmptyStack<T>, n: number): NonEmptyStack<T> {
    while (n--) {
        const next = s.rest;
        if (!nonEmpty(next)) throw new Error("dropNonEmpty popped too far");
        s = next;
    }
    return s;
}

export function depth<T>(s: Stack<T>): number {
    let count = 0;
    while (s !== null) {
        count++;
        s = s.rest;
    }
    return count;
}

export function toArray<T>(s: Stack<T>): T[] {
    const result = [];
    while (s !== null) {
        result.push(s.item);
        s = s.rest;
    }
    return result;
}
