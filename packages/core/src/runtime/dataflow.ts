/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

// Property-based "dataflow"

import { FlexSet, FlexMap, Canonicalizer, is } from '@preserves/core';
import * as MapSet from './mapset';

export interface ObservingGraph<ObjectId> {
    recordObservation(objectId: ObjectId): void;
    recordDamage(objectId: ObjectId): void;
    suppressCycleWarning(objectId: ObjectId): void;
}

export class Graph<SubjectId, ObjectId> implements ObservingGraph<ObjectId> {
    readonly edgesForward: FlexMap<ObjectId, FlexSet<SubjectId>>;
    readonly edgesReverse: FlexMap<SubjectId, FlexSet<ObjectId>>;
    damagedNodes: FlexSet<ObjectId>;
    currentSubjectId: SubjectId | undefined;
    cyclicWarningSuppressed: FlexSet<ObjectId> | undefined;

    constructor(
        public readonly subjectIdCanonicalizer: Canonicalizer<SubjectId>,
        public readonly objectIdCanonicalizer: Canonicalizer<ObjectId>,
        public formatCycle: (group: FlexSet<ObjectId>) => any = g => g,
    ) {
        this.edgesForward = new FlexMap(objectIdCanonicalizer);
        this.edgesReverse = new FlexMap(subjectIdCanonicalizer);
        this.damagedNodes = new FlexSet(objectIdCanonicalizer);
    }

    withSubject<T>(subjectId: SubjectId | undefined, f: () => T): T {
        let oldSubjectId = this.currentSubjectId;
        this.currentSubjectId = subjectId;
        let result: T;
        try {
            result = f();
        } catch (e) {
            this.currentSubjectId = oldSubjectId;
            throw e;
        }
        this.currentSubjectId = oldSubjectId;
        return result;
    }

    recordObservation(objectId: ObjectId) {
        if (this.currentSubjectId !== void 0) {
            MapSet.add(this.edgesForward, objectId, this.currentSubjectId, this.subjectIdCanonicalizer);
            MapSet.add(this.edgesReverse, this.currentSubjectId, objectId, this.objectIdCanonicalizer);
        }
    }

    recordDamage(objectId: ObjectId) {
        this.damagedNodes.add(objectId);
    }

    forgetSubject(subjectId: SubjectId) {
        const subjectObjects = this.edgesReverse.get(subjectId) ?? [] as Array<ObjectId>;
        this.edgesReverse.delete(subjectId);
        subjectObjects.forEach((oid: ObjectId) => MapSet.del(this.edgesForward, oid, subjectId));
    }

    observersOf(objectId: ObjectId): Array<SubjectId> {
        const subjects = this.edgesForward.get(objectId);
        if (subjects === void 0) return [];
        return Array.from(subjects);
    }

    suppressCycleWarning(objectId: ObjectId): void {
        if (this.cyclicWarningSuppressed === void 0) {
            this.cyclicWarningSuppressed = new FlexSet(this.objectIdCanonicalizer);
        }
        this.cyclicWarningSuppressed.add(objectId);
    }

    repairDamage(repairNode: (subjectId: SubjectId) => void) {
        let repairedThisRound = new FlexSet(this.objectIdCanonicalizer);
        let suppressed = new FlexSet(this.objectIdCanonicalizer);
        while (true) {
            if (this.cyclicWarningSuppressed !== void 0) {
                suppressed = suppressed.union(this.cyclicWarningSuppressed);
                this.cyclicWarningSuppressed = void 0;
            }

            let workSet = this.damagedNodes;
            this.damagedNodes = new FlexSet(this.objectIdCanonicalizer);

            const alreadyDamaged = workSet.intersect(repairedThisRound).subtract(suppressed);
            if (alreadyDamaged.size > 0) {
                console.warn('Cyclic dependencies involving', this.formatCycle(alreadyDamaged));
            }

            workSet = workSet.subtract(repairedThisRound);
            repairedThisRound = repairedThisRound.union(workSet);

            if (workSet.size === 0) break;

            const updatedSubjects = new FlexSet(this.subjectIdCanonicalizer);
            workSet.forEach(objectId => {
                this.observersOf(objectId).forEach((subjectId: SubjectId) => {
                    if (!updatedSubjects.has(subjectId)) {
                        updatedSubjects.add(subjectId);
                        this.forgetSubject(subjectId);
                        this.withSubject(subjectId, () => repairNode(subjectId));
                    }
                });
            });
        }
    }
}

let __nextCellId = 0;

export abstract class Cell {
    readonly id: string = '' + (__nextCellId++);
    readonly graph: ObservingGraph<Cell>;
    __value: unknown;

    static readonly canonicalizer: Canonicalizer<Cell> = v => v.id;

    constructor(graph: ObservingGraph<Cell>, initial: unknown) {
        this.graph = graph;
        this.__value = initial;
    }

    suppressCycleWarning() {
        this.graph.suppressCycleWarning(this);
    }

    observe(): unknown {
        this.graph.recordObservation(this);
        return this.__value;
    }

    changed() {
        this.graph.recordDamage(this);
    }

    update(newValue: unknown) {
        if (!this.valuesEqual(this.__value, newValue)) {
            this.changed();
            this.__value = newValue;
        }
    }

    abstract valuesEqual(v1: unknown, v2: unknown): boolean;

    toString(): string {
        return `𝐶<${this.__value}>`;
    }
}

export abstract class TypedCell<V> extends Cell {
    constructor(graph: ObservingGraph<Cell>, initial: V) {
        super(graph, initial);
    }

    get value(): V {
        return this.observe() as V;
    }

    set value(v: V) {
        this.update(v);
    }

    abstract valuesEqual(v1: V, v2: V): boolean;
}

export class Field<V> extends TypedCell<V> {
    readonly name: string | undefined;

    constructor(graph: ObservingGraph<Cell>, initial: V, name?: string) {
        super(graph, initial);
        this.name = name;
    }

    valuesEqual(v1: V, v2: V): boolean {
        return is(v1, v2);
    }

    toString(): string {
        return `𝐹<${this.name}=${this.__value}>`;
    }
}
