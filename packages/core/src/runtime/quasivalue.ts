/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2021-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { AnyValue, Ref } from './actor.js';
import { Pattern, toPattern, GroupType, fromGroupType } from '../gen/dataspacePatterns.js';
import * as P from './pattern.js';
import { RecordConstructorInfo, is, Record, JsDictionary, Float, Double, FromJSOptions, fromJS_options, DoubleFloat } from '@preserves/core';
import { Meta, Type, GenType, SchemaDefinition } from '@preserves/schema';

export type QuasiValueMethod = (... args: QuasiValue[]) => QuasiValue;

export type QuasiValueConstructorInfo =
    | { constructorInfo: RecordConstructorInfo<AnyValue, Ref> }
    | { schema(): SchemaDefinition }
    | { quasiValue: QuasiValueMethod }
;

export type QuasiValue =
    | { type: 'bind', inner: QuasiValue }
    | { type: 'discard' }
    | { type: 'lit', value: AnyValue }
    | { type: 'group', groupType: GroupType, entries: [AnyValue, QuasiValue][] }
    | { type: 'unquote', unquoted: QuasiValue }
;

export function bind(p?: QuasiValue): QuasiValue {
    return { type: 'bind', inner: p ?? _ };
}

bind.quasiValue = (inner?: QuasiValue) => rec(Symbol.for('bind'), inner ?? _);

export function discard(): QuasiValue {
    return { type: 'discard' };
}

discard.quasiValue = () => rec(Symbol.for('_'));

export const _ = discard();

export function lit(value: AnyValue): QuasiValue {
    return { type: 'lit', value };
}

lit.quasiValue = (q: QuasiValue) => rec(Symbol.for('lit'), q);

export function drop_lit(patValue: AnyValue): AnyValue | null;
export function drop_lit<R>(patValue: AnyValue, parser: (v: AnyValue) => R): R | null;
export function drop_lit<R>(patValue: AnyValue, parser?: (v: AnyValue) => R): any {
    const pat = toPattern(patValue);
    if (pat === void 0) return null;
    const val = P.drop_lit(pat);
    if (val === null) return null;
    return parser === void 0 ? val : parser(val);
}

export function rec(label: AnyValue, ... items: QuasiValue[]): QuasiValue {
    const literals = items.flatMap(i => i.type === 'lit' ? [i.value] : []);
    if (literals.length === items.length) {
        return lit(Record(label, literals));
    } else {
        const entries = items.map((v, i) => [i, v] as [number, QuasiValue]);
        return { type: 'group', groupType: GroupType.rec(label), entries };
    }
}

export function arr(... items: QuasiValue[]): QuasiValue {
    const literals = items.flatMap(i => i.type === 'lit' ? [i.value] : []);
    if (literals.length === items.length) {
        return lit(literals);
    } else {
        const entries = items.map((v, i) => [i, v] as [number, QuasiValue]);
        return { type: 'group', groupType: GroupType.arr(), entries };
    }
}

export function dict(... entries: [AnyValue, QuasiValue][]): QuasiValue {
    return { type: 'group', groupType: GroupType.dict(), entries };
}

export function quote(quoted: QuasiValue): QuasiValue {
    switch (quoted.type) {
        case 'bind': return quote(bind.quasiValue(quoted.inner));
        case 'discard': return quote(discard.quasiValue());
        case 'lit': return rec(Symbol.for('lit'), lit(quoted.value));
        case 'group':
            return rec(Symbol.for('group'), lit(fromGroupType(quoted.groupType)), dict(
                ... quoted.entries.map(([k, qq]) => [k, quote(qq)] as [AnyValue, QuasiValue])));
        case 'unquote': return quoted.unquoted;
    }
}

export function unquote(unquoted: QuasiValue): QuasiValue {
    return { type: 'unquote', unquoted };
}

function entriesSeq<V>(entries: [AnyValue, V][], defaultValue: V): V[] {
    let maxKey = -1;
    const result: V[] = [];
    for (const [k, q] of entries) {
        if (typeof k !== 'number') throw new Error("Invalid sequence quasivalue entries key");
        result[k] = q;
        maxKey = Math.max(maxKey, k);
    }
    for (let i = 0; i < maxKey + 1; i++) {
        if (result[i] === void 0) result[i] = defaultValue;
    }
    return result;
}

export function ctor(info: QuasiValueConstructorInfo, ... items: QuasiValue[]): QuasiValue {
    if ('constructorInfo' in info) {
        return rec(info.constructorInfo.label, ... items);
    } else if ('schema' in info) {
        const definfo = info.schema();
        const schema = Meta.asSchema(definfo.schema);
        const def = JsDictionary.get(schema.definitions, definfo.definitionName)!;
        const defNameStr = definfo.definitionName.description!;
        const ctorArgs = items.slice();

        let defType = GenType.typeForDefinition(r => Type.Type.ref(r.name.description!, null), def);
        // ^ defType is updated when we are facing a variant in a union

        function qLiteral(p: Meta.NamedPattern): AnyValue {
            if (p._variant === 'anonymous' &&
                p.value._variant === 'SimplePattern' &&
                p.value.value._variant === 'lit')
            {
                return p.value.value.value as AnyValue; // TODO ughhhh!!
            }

            if (p._variant === 'named') {
                const qv = qLookup(p.value);
                if (qv.type === 'lit') {
                    return qv.value;
                }
            }

            throw new Error("Only very simple record labels are supported");
        }

        function qNamed(p: Meta.NamedPattern): QuasiValue {
            switch (p._variant) {
                case 'anonymous': return qPattern(p.value);
                case 'named': return qLookup(p.value);
            }
        }

        function qNamedSimple(p: Meta.NamedSimplePattern): QuasiValue {
            switch (p._variant) {
                case 'anonymous': return qSimple(p.value);
                case 'named': return qLookup(p.value);
            }
        }

        function qLookup(b: Meta.Binding): QuasiValue {
            let d: QuasiValue;
            switch (ctorArgs.length) {
                case 0:
                    // Previously, this was an error:
                    //   throw new Error(`Missing dictionary argument to ${defNameStr}`);
                    // but I think it actually makes sense to just treat it as a full-on discard.
                    d = _;
                    break;
                case 1:
                    d = ctorArgs[0];
                    break;
                default:
                    throw new Error(`Too many arguments to ${defNameStr}`);
            }
            if (defType.kind === 'record' && defType.fields.size !== 1) {
                if (d.type === 'discard') {
                    return d;
                }
                if (d.type !== 'group' || d.groupType._variant !== 'dict') {
                    throw new Error(`Dictionary argument needed to ${defNameStr}`);
                }
                for (const [k, p] of d.entries) {
                    if (is(k, b.name.description!)) {
                        return p;
                    }
                }
                return _;
            } else {
                return d;
            }
        }

        function qPattern(p: Meta.Pattern): QuasiValue {
            switch (p._variant) {
                case 'SimplePattern': return qSimple(p.value);
                case 'CompoundPattern': return qCompound(p.value);
            }
        }

        function qSimple(p: Meta.SimplePattern): QuasiValue {
            switch (p._variant) {
                case 'lit':
                    return lit(p.value as AnyValue); // TODO: hate this cast
                case 'any':
                case 'atom':
                case 'embedded':
                case 'seqof':
                case 'setof':
                case 'dictof':
                case 'Ref':
                    throw new Error("Cannot synthesize value for simple pattern");
            }
        }

        function qCompound(p: Meta.CompoundPattern): QuasiValue {
            switch (p._variant) {
                case 'rec':
                    switch (p.fields._variant) {
                        case 'named':
                            return rec(qLiteral(p.label), ... qArr(qLookup(p.fields.value)));
                        case 'anonymous':
                            return rec(qLiteral(p.label), ... qArr(qPattern(p.fields.value)));
                    }
                case 'tuple':
                    return arr(... p.patterns.map(qNamed));
                case 'tuplePrefix':
                    throw new Error("Cannot use tuplePrefix pattern as dataspace pattern");
                case 'dict': {
                    const entries: [AnyValue, QuasiValue][] = [];
                    p.entries.forEach((pp, k) => entries.push([k as AnyValue, // TODO ugh!!
                                                               qNamedSimple(pp)]));
                    return dict(... entries);
                }
            }
        }

        function qArr(q: QuasiValue): QuasiValue[] {
            if (q.type === 'group' && q.groupType._variant === 'arr') {
                return entriesSeq(q.entries, discard());
            } else if (q.type === 'lit' && Array.isArray(q.value)) {
                return q.value.map(lit);
            } else {
                throw new Error("Array of quasivalues needed");
            }
        }

        function qTopPattern(p: Meta.Pattern): QuasiValue {
            switch (p._variant) {
                case 'SimplePattern':
                    if (defType.kind === 'unit') {
                        return qSimple(p.value);
                    } else {
                        if (ctorArgs.length === 0) {
                            throw new Error(`Missing argument to ${defNameStr}`);
                        }
                        return ctorArgs[0];
                    }
                case 'CompoundPattern':
                    return qCompound(p.value);
            }
        }

        switch (def._variant) {
            case 'or': {
                const variant = definfo.variant?.description;
                if (variant === void 0) {
                    throw new Error("Cannot use union definition as pattern");
                }
                if (defType.kind !== 'union') {
                    throw new Error(`Non-union type for union definition ${defNameStr}`);
                }
                for (const p of [def.pattern0, def.pattern1, ...def.patternN]) {
                    if (p.variantLabel === variant) {
                        defType = defType.variants.get(variant)!;
                        return qTopPattern(p.pattern);
                    }
                }
                throw new Error(`Unknown variant ${variant} in definition ${defNameStr}`);
            }
            case 'and':
                throw new Error("Cannot use intersection definition as pattern");
            case 'Pattern':
                return qTopPattern(def.value);
        }
    } else if ('quasiValue' in info) {
        return info.quasiValue(... items);
    } else {
        ((info: never) => {
            console.error('ctor() in quasivalue.ts cannot handle', info);
            throw new Error("INTERNAL ERROR");
        })(info);
    }
}

export function finish(q: QuasiValue): Pattern {
    // console.log('--------------------------');
    // console.log(q);
    const p = walk(q);
    // console.log(p);
    return p;
}

function walk(q: QuasiValue): Pattern {
    switch (q.type) {
        case 'bind': return P.bind(walk(q.inner));
        case 'discard': return P._;
        case 'lit': return P.lit(q.value);
        case 'group': switch (q.groupType._variant) {
            case 'arr': return P.arr(... entriesSeq(q.entries, discard()).map(walk));
            case 'rec': return P.rec(q.groupType.label, ... entriesSeq(q.entries, discard()).map(walk));
            case 'dict': return P.dict(... q.entries.map(
                ([k, qq]) => [k, walk(qq)] as [AnyValue, Pattern]));
        }
        case 'unquote': throw new Error('Unexpected unquote in QuasiValue');
    }
}

//---------------------------------------------------------------------------
// Limited support for literal non-integers in patterns
//  - literal 1.234 is given to fromJS with an option to promote non-integers to Double
//  - use of Double as a constructor in a pattern coerces integers to Double

const looseFromJSOptions: FromJSOptions<Ref> = {
    onNonInteger(n) { return Double(n); },
};

export function litFromJS(x: any): QuasiValue {
    return lit(fromJS_options(x, looseFromJSOptions));
}

declare module '@preserves/core' {
    namespace Double {
        const quasiValue: QuasiValueMethod;
    }
}
(Double as { quasiValue: QuasiValueMethod }).quasiValue = (n: QuasiValue) => {
    const value = n.type === 'lit' ? CoerceDouble.__from_preserve__(n.value) : void 0;
    if (value === void 0) {
        throw new Error("Double, used as a pattern, expects a pattern over a specific number as its argument");
    }
    return { ... n, value };
};

//---------------------------------------------------------------------------
// Limited support for accepting Double or integer in patterns
//  - used as a type guard in a pattern will coerce integers to Double
//  - used as a function will do likewise -- like Double() itself does

export function CoerceDouble(v: number | bigint | Float): DoubleFloat {
    return Double(v);
}
CoerceDouble.__from_preserve__ = (v: AnyValue) => {
    if (typeof v === 'number' || typeof v === 'bigint' || Float.isFloat(v)) {
        return Double(v);
    }
    return void 0;
};
