/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Assertable, Ref } from './actor.js';
import { ctor, dict, QuasiValue } from './quasivalue.js';
import * as R from '../gen/rpc.js';
import { fromJS  } from '@preserves/core';

export { Result } from '../gen/rpc.js';

export function Question<Q extends Assertable>(q: Q): R.Question<Ref> {
    return R.Question(fromJS<Ref>(q));
}

export namespace Question {
    export function quasiValue(q: QuasiValue): QuasiValue {
        return ctor(R.Question, q);
    }
}

export function Answer<Q extends Assertable, A extends Assertable>(q: Q, a: A): R.Answer<Ref> {
    return R.Answer({ request: fromJS<Ref>(q), response: fromJS<Ref>(a) });
}

export namespace Answer {
    export function quasiValue(q: QuasiValue, a: QuasiValue): QuasiValue {
        return ctor(R.Answer, dict(
            ["request", q],
            ["response", a]));
    }
}

export function resultOk<V extends Assertable>(v: V): R.Result<Ref> {
    return R.Result.ok(fromJS<Ref>(v));
}

export namespace resultOk {
    export function quasiValue(v: QuasiValue): QuasiValue {
        return ctor(R.Result.ok, v);
    }
}

export function resultError<E extends Assertable>(e: E): R.Result<Ref> {
    return R.Result.error(fromJS<Ref>(e));
}

export namespace resultError {
    export function quasiValue(e: QuasiValue): QuasiValue {
        return ctor(R.Result.error, e);
    }
}
