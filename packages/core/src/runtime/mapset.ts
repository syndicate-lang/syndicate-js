/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

// Utilities for Maps of Sets

import { FlexSet, FlexMap, Canonicalizer } from '@preserves/core';

export function add<K,V>(m: FlexMap<K, FlexSet<V>>, k: K, v: V, c: Canonicalizer<V>) {
    let s = m.get(k);
    if (!s) {
        s = new FlexSet(c);
        m.set(k, s);
    }
    s.add(v);
}

export function del<K,V>(m: FlexMap<K, FlexSet<V>>, k: K, v: V) {
    const s = m.get(k);
    if (!s) return;
    s.delete(v);
    if (s.size === 0) m.delete(k);
}
