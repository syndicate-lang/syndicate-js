/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

export * from '@preserves/core';

export * as Schemas from './schemas.js';
export { Observe, asObserve, toObserve, fromObserve } from './gen/dataspace.js';
export * as DataspacePatterns from './gen/dataspacePatterns.js';

export * from './runtime/actor.js';
export * from './runtime/bag.js';
export * as Dataflow from './runtime/dataflow.js';
export { Field } from './runtime/dataflow.js';
export * from './runtime/dataspace.js';
export * as Pattern from './runtime/pattern.js';
export * as QuasiValue from './runtime/quasivalue.js';
export { CoerceDouble } from './runtime/quasivalue.js';
export * from './runtime/randomid.js';
export * as Rewrite from './runtime/rewrite.js';
export * from './runtime/rpc.js';
export * as Rpc from './runtime/rpc.js';
export * as Skeleton from './runtime/skeleton.js';
export * from './runtime/space.js';
export * from './runtime/supervise.js';

export * as SaltyCrypto from 'salty-crypto';
export * as Cryptography from './transport/cryptography.js';
export * as WireProtocol from './transport/protocol.js';
export * as Membrane from './transport/membrane.js';
export * as Relay from './transport/relay.js';
export * as Sturdy from './transport/sturdy.js';

import { randomId } from './runtime/randomid.js';

// These aren't so much "Universal" as they are "VM-wide-unique".
let uuidIndex = 0;
let uuidInstance = randomId(8);
export function genUuid(prefix: string = '__@syndicate'): string {
    return `${prefix}_${uuidInstance}_${uuidIndex++}`;
}
