/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { GenericEmbedded, KeyedSet } from '@preserves/core';
import { Bag, ChangeDescription } from '../src/runtime/bag';

describe('bag', () => {
    it('should be initializable from a set', () => {
        const b = new Bag(new KeyedSet<GenericEmbedded, string>(['a', 'b', 'c']));
        expect(b.size).toBe(3);
        expect(b.get('a')).toBe(1);
        expect(b.get('z')).toBe(0);
    });

    it('should be mutable', () => {
        const b = new Bag();
        b.change('a', 1);
        b.change('a', 1);
        expect(b.get('a')).toBe(2);
    });

    it('should count up', () => {
        const b = new Bag();
        expect(b.change('a', 1)).toBe(ChangeDescription.ABSENT_TO_PRESENT);
        expect(b.change('a', 1)).toBe(ChangeDescription.PRESENT_TO_PRESENT);
        expect(b.get('a')).toBe(2);
        expect(b.get('z')).toBe(0);
    });

    it('should count down', () => {
        const b = new Bag(new KeyedSet<GenericEmbedded, string>(['a']));
        expect(b.change('a', 1)).toBe(ChangeDescription.PRESENT_TO_PRESENT);
        expect(b.change('a', -1)).toBe(ChangeDescription.PRESENT_TO_PRESENT);
        expect(b.size).toBe(1);
        expect(b.change('a', -1)).toBe(ChangeDescription.PRESENT_TO_ABSENT);
        expect(b.size).toBe(0);
        expect(b.get('a')).toBe(0);
        expect(b.get('z')).toBe(0);
        expect(b.change('a', -1)).toBe(ChangeDescription.ABSENT_TO_PRESENT);
        expect(b.size).toBe(1);
        expect(b.get('a')).toBe(-1);
    });

    it('should be clamped', function() {
        const b = new Bag(new KeyedSet(['a']));
        for (let i = 0; i < 4; i++) b.change('a', -1, true);
        expect(b.size).toBe(0);
        expect(b.get('a')).toBe(0);
    });
});
