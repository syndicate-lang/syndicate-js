/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { SyndicateRollup } from '../../rollup.js';
const r = new SyndicateRollup('syndicate', { globalName: 'Syndicate' });
export default [
  r.config('lib/index.js', r.umd, {
    external: ['@preserves/core'],
    output: {
      globals: {
        '@preserves/core': 'Preserves',
      },
    },
  }),
  r.config('lib/index.js', r.es6, {
    external: ['@preserves/core'],
    resolve: {
      moduleDirectories: ['node_modules'],
    },
  }),
];
