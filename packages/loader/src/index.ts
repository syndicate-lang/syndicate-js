/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Module } from 'node:module';
import { compile, Syntax } from '@syndicate-lang/compiler';
import Pos = Syntax.Pos;

import * as Syndicate from '@syndicate-lang/core';
Object.defineProperty(globalThis, 'currentSyndicateTarget', {
    value: () => Syndicate.Dataspace.local,
    writable: false,
});

const translateable_paths: { [key: string]: true } = {};

const _oldCompile = (Module.prototype as any)._compile;
function _compile(this: typeof Module, source: string, name: string) {
    if (name in translateable_paths) {
        // console.log('Syndicate-compiling', name);
        const compiled = compile({
            source,
            name,
            module: 'require',
            emitError: (message: string, start: Pos | undefined, end: Pos | undefined) => {
                console.error('Syndicate compiler error', start, end, message);
            },
        });
        // console.log({ name, compiled: compiled.text });
        const result = _oldCompile.call(this, compiled.text, name);
        // console.log(result);
        return result;
    } else {
        return _oldCompile.call(this, source, name);
    }
}
(Module.prototype as any)._compile = _compile;

const _oldLoad = (Module as any)._load;
function _load(request: string, parent: string, isMain: boolean) {
    // console.log('Syndicate-loading', request, parent);
    if (request[0] === '/' || request[0] === '.') {
        const resolved = (Module as any)._resolveFilename(request, parent, isMain);
        translateable_paths[resolved] = true;
    }
    return _oldLoad(request, parent, isMain);
}
(Module as any)._load = _load;
