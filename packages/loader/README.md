# @syndicate-lang/loader

Node pre-require module that hooks `Module._load` and `Module.prototype._compile` to translate
`require`d modules on-the-fly from Syndicate/JS syntax to plain CommonJS:

```shell
node -r @syndicate-lang/loader FILENAME.js
```

You can also use it to run standalone Syndicate/JS scripts:

```javascript
#!/usr/bin/env -S node -r @syndicate-lang/loader

spawn named 'test' {
  console.log('hello');
  on stop console.log('goodbye');
}
```

## Licence

@syndicate-lang/loader, Syndicate/JS node.js loader hook
Copyright (C) 2023 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
