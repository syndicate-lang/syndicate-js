/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { preserves, Double, Observe, floatValue, Turn, Ref, Schemas, Dataspace } from "@syndicate-lang/core";
import { QuasiValue as Q } from "@syndicate-lang/core";

export const PeriodicTick = Schemas.timer.PeriodicTick;
export type PeriodicTick = Schemas.timer.PeriodicTick;

export const LaterThan = Schemas.timer.LaterThan;
export type LaterThan = Schemas.timer.LaterThan;

export function sleep(ds: Ref, seconds: number, cb: () => void): void {
    react {
        at ds {
            const deadline = Double(+(new Date()) / 1000.0 + seconds);
            stop on asserted LaterThan(deadline) => {
                cb();
            }
        }
    }
}

export function boot(ds = Dataspace.local) {
    spawn named 'timer/PeriodicTick' {
        at ds {
            during Observe({
                "pattern": :pattern PeriodicTick(\Q.lit($interval: Double))
            }) => spawn named (preserves`PeriodicTick(${interval})`) {
                const thisFacet = Turn.activeFacet;
                thisFacet.preventInertCheck();
                const handle = setInterval(() => thisFacet.turn(() => {
                    send message PeriodicTick(floatValue(interval));
                }), floatValue(interval) * 1000.0);
                on stop clearInterval(handle);
            }
        }
    }

    spawn named 'timer/LaterThan' {
        at ds {
            during Observe({
                "pattern": :pattern LaterThan(\Q.lit($deadline: Double))
            }) => spawn named (preserves`LaterThan(${deadline})`) {
                const thisFacet = Turn.activeFacet;
                thisFacet.preventInertCheck();
                let delta = floatValue(deadline) * 1000.0 - (+(new Date()));
                const ready = () => {
                    react {
                        assert LaterThan(floatValue(deadline));
                    }
                };
                if (delta <= 0) {
                    ready();
                } else {
                    let handle: any | null = setTimeout(() =>
                        thisFacet.turn(() => { handle = null; ready(); }), delta);
                    on stop if (handle) {
                        clearTimeout(handle);
                        handle = null;
                    }
                }
            }
        }
    }
}
