/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Items } from './tokens';
import { Pos, startPos } from './position';
import { laxRead, LaxReadOptions } from './reader';
import * as M from './matcher';

const substPat = M.scope((o: { pos: Pos }) =>
    M.seq(M.atom('$'),
          M.seq(M.bind(o, 'pos', M.pos), M.group('{', M.end, { skipSpace: false }))));

export type Substitution = Items | string;

function toItems(readOptions: LaxReadOptions, s: Substitution, pos: Pos): Items {
    return typeof s === 'string' ? laxRead(s, { ... readOptions, start: pos, synthetic: true }) : s;
}

export type TemplateFunction = (consts: TemplateStringsArray, ... vars: Substitution[]) => Items;

export class Templates {
    readonly sources: { [name: string]: string } = {};
    readonly defaultPos: Pos;
    recordSources = false;
    readonly readOptions: LaxReadOptions;

    constructor(defaultPos: Pos = startPos(null), readOptions: LaxReadOptions = {}) {
        this.defaultPos = defaultPos;
        this.readOptions = readOptions;
    }

    template(start0: Pos | string = this.defaultPos, context: M.ItemContext = null): TemplateFunction {
        const start = (typeof start0 === 'string') ? startPos(start0) : start0;
        return (consts, ... vars) => {
            const sourcePieces = [consts[0]];
            for (let i = 1; i < consts.length; i++) {
                sourcePieces.push('${}');
                sourcePieces.push(consts[i]);
            }
            const source = sourcePieces.join('');
            if (this.recordSources) {
                if (start.name !== null) {
                    if (start.name in this.sources && this.sources[start.name] !== source) {
                        throw new Error(`Duplicate template name: ${start.name}`);
                    }
                    this.sources[start.name] = source;
                }
            }
            let i = 0;
            return M.replace(laxRead(source, { ... this.readOptions,
                                               start,
                                               extraDelimiters:
                                                 (this.readOptions.extraDelimiters ?? '') + '$',
                                               synthetic: true,
                                             }),
                             context,
                             substPat,
                             sub => toItems(this.readOptions, vars[i++], sub.pos));
        };
    }

    sourceFor(name: string): string | undefined {
        return this.sources[name];
    }
}

export function joinItems(itemss: Items[],
                          separator0: Substitution = '',
                          readOptions: LaxReadOptions = {}): Items
{
    if (itemss.length === 0) return [];
    const separator = toItems(readOptions, separator0, startPos(null));
    const acc: Items = [... itemss[0]];
    for (let i = 1; i < itemss.length; i++) {
        acc.push(... separator, ... itemss[i]);
    }
    return acc;
}

export function commaJoin(itemss: Items[]): Items {
    return joinItems(itemss, ', ');
}

export const anonymousTemplate = (new Templates()).template();

// const lib = new Templates();
// const t = (o: {xs: Items}) => lib.template('testTemplate')`YOYOYOYO ${o.xs}><`;
// console.log(t({xs: lib.template()`hello there`}));
