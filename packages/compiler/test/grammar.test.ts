/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2024-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Grammar, Syntax } from '../src/index';
import './test-utils';

describe('statement boundary', () => {
    function stmt(input: string): [string, string] | null {
        const parser = new Grammar.SyndicateParser();
        const tree = Syntax.laxRead(input);
        const items: Syntax.Items = [];
        const r = parser.statement(items)(new Syntax.ArrayList(tree, '{'));
        if (r === null) return null;
        return [Syntax.itemText(items), Syntax.itemText(r[1].toArray())];
    }

    it('should include semicolon', () => {
        expect(stmt('i am a statement ; ')).toEqual(['i am a statement;', ' ']);
    });

    it('should include newline', () => {
        expect(stmt('i am a statement \n ')).toEqual(['i am a statement\n', ' ']);
    });

    it('should include closing brace on the same line', () => {
        // Note that `" remainder is in outer group"` is discarded by `laxRead`.
        expect(stmt('i am a statement } remainder is in outer group'))
            .toEqual(['i am a statement', '']);
    });
});
