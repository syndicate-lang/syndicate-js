/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2016-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { SyndicateRollup } from '../../rollup.js';
const r = new SyndicateRollup('syndicate-compiler', { globalName: 'SyndicateCompiler' });
export default [
  r.config('lib/index.js', r.umd),
  r.config('lib/index.js', r.es6),
];
