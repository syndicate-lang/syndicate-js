/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

const __SYNDICATE__ = Syndicate;
const currentSyndicateTarget = () => Syndicate.Dataspace.local;

(() => {
  async function translateScripts() {
    if (SchemaReady) await SchemaReady;

    const syndicateScripts =
          Array.from(document.getElementsByTagName('script'))
          .filter(s => (s.type === 'text/javascript+syndicate' ||
                        s.type === 'syndicate' ||
                        s.type === 'module+syndicate'));

    for (const script of syndicateScripts) {
      const isModule = script.type === 'module+syndicate';

      const sourceUrl = script.src || script.getAttribute('data-src') || false;
      const sourceCode = sourceUrl ? await (await fetch(sourceUrl)).text() : script.innerHTML;

      const compilationResult = SyndicateCompiler.compile({
        name: sourceUrl || script.id || 'anonymous-script-tag',
        source: sourceCode,
        module: isModule ? 'es6' : 'none',
        runtime: isModule ? void 0 : 'Syndicate',
        emitError: console.error,
      });

      const sourceMap = { ... compilationResult.map };
      sourceMap.sourcesContent = [sourceCode];
      const formattedSourceMap = '\n//# sourceMappingURL=data:application/json;base64,' +
            Syndicate.Bytes.from(JSON.stringify(sourceMap)).toBase64();
      const finalOutput = compilationResult.text + formattedSourceMap;

      // console.log(finalOutput);

      const replacement = document.createElement('script');
      replacement.text = finalOutput;
      if (isModule) replacement.type = 'module';
      script.parentNode.replaceChild(replacement, script);
    }
  }

  window.addEventListener('DOMContentLoaded', translateScripts);
})();
