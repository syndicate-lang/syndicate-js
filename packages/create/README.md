# @syndicate-lang/create

<!-- Use `npx @syndicate-lang/create DIRECTORY` or `npm init @syndicate-lang DIRECTORY`: -->

To create a new program/library using Syndicate/js, use `yarn create @syndicate-lang DIRECTORY`:

```shell
yarn create @syndicate-lang myprogram
cd myprogram
yarn
yarn serve
```

Then visit <http://localhost:8000/>. By default, the generated app expects a
[syndicate-server](https://git.syndicate-lang.org/syndicate-lang/syndicate-rs/) to be running
on port 9001 exposing a dummy capability to a dataspace entity. Suitable configuration can be
found in the [`syndicate-server.config.pr`](template/syndicate-server.config.pr) file. Start
the server like this:

```shell
syndicate-server -c ./syndicate-server.config.pr
```

If you aren't writing a web app in Syndicate, you can remove the following files from the
generated module as well as the `serve` script from package.json:

 - `index.html`
 - `style.css`
 - `rollup.config.js`
 - `syndicate-server.config.pr`

In that case, your `index.ts` can be as simple as

```typescript
import { Dataspace } from "@syndicate-lang/core";
Dataspace.boot(ds => {
    /* your app goes here */
});
```

If you don't need to define any Preserves
[schemas](https://preserves.dev/preserves-schema.html) in your application,
remove the `protocols/` directory as well as the various `regenerate` scripts and any mention
of the `src/gen/` directory from `package.json`.
