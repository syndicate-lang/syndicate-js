/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import yargs from 'yargs/yargs';

import fs from 'fs';
import path from 'path';
import unzip from 'unzipper';

export type CommandLineArguments = {
    directory: string,
    package?: string,
};

export async function main(argv: string[]) {
    const options: CommandLineArguments = yargs(argv)
        .command('$0 <directory>',
                 'Create a new package configured for TypeScript and Syndicate/js',
                 yargs => yargs
                     .positional('directory', {
                         type: 'string',
                         description: 'Directory in which to place the new package',
                     }).demandOption('directory')
                     .option('package', {
                         alias: 'p',
                         type: 'string',
                         description: 'Package name to create; defaults to directory given',
                     }),
                 argv => argv)
        .argv;

    const directory = path.resolve(options.directory);
    const packageName = options.package ?? path.basename(directory);
    console.log(`Creating package ${packageName} in ${directory}.`);

    fs.mkdirSync(directory, { recursive: true });
    fs.createReadStream(path.join(__dirname, 'syndicate-template.zip')).pipe(
        unzip.Extract({ path: directory }).on('close', (err: any) => {
            if (err) throw err;
            const packageJsonPath = path.join(directory, 'package.json');
            const p = Object.assign({
                name: packageName,
            }, require(packageJsonPath));
            fs.writeFileSync(packageJsonPath, JSON.stringify(p, null, 2));
        }));
}
