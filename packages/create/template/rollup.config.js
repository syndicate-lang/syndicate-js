/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import sourcemaps from 'rollup-plugin-sourcemaps';

export default {
  input: 'lib/index.js',
  plugins: [sourcemaps()],
  external: [
    '@preserves/core',
    '@syndicate-lang/core',
    '@syndicate-lang/html2',
    '@syndicate-lang/ws-relay',
  ],
  output: {
    file: 'index.js',
    format: 'umd',
    name: 'Main',
    sourcemap: true,
    globals: {
      '@preserves/core': 'Preserves',
      '@syndicate-lang/core': 'Syndicate',
      '@syndicate-lang/html2': 'SyndicateHtml2',
      '@syndicate-lang/ws-relay': 'SyndicateWsRelay',
    },
  },
};
