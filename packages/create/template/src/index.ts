/// SPDX-License-Identifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Copyright © 2023-2025 Tony Garnock-Jones <tonyg@leastfixedpoint.com>

import { Dataspace, Ref, Sturdy, Reader, Schemas, randomId, fromJS, Question, resultOk, Answer } from "@syndicate-lang/core";
import { boot as bootHtml, Widget } from "@syndicate-lang/html2";
import wsRelay from "@syndicate-lang/ws-relay";
import { ExampleDefinition } from './gen/example';
import G = Schemas.gatekeeper;

export function main() {
    Dataspace.boot(ds => {
        bootHtml(ds);
        wsRelay.boot(ds, true /* remove this `true` to turn off client/server debug logging */);
        bootApp(ds);
    });
}

function bootApp(ds: Ref) {
    spawn named 'app' {
        at ds {
            /*
             * This example expects a syndicate-server instance running on port 8000 on the
             * server hosting index.html, exposing a dataspace entity via a capability called
             * `"syndicate"` with empty "secret". See syndicate-server.config.pr.
             */

            const this_instance = randomId(16);

            const route = G.Route<Ref>({
                "transports": [fromJS(Schemas.transportAddress.WebSocket(
                    `ws://${document.location.hostname}:8000/`))],
                "pathSteps": [G.asPathStep(fromJS(Sturdy.asSturdyRef(
                        new Reader<Ref>(
                            '<ref {oid: "syndicate" sig: #[acowDB2/oI+6aSEC3YIxGg==]}>').next())))],
            });

            assert Question(G.ResolvePath(route));
            during Answer(G.ResolvePath(route), resultOk(G.ResolvedPath({
                responderSession: $remoteDs: Ref,
            }))) => {
                at remoteDs {
                    assert ExampleDefinition(this_instance);
                    during ExampleDefinition($who: string) => {
                        new Widget(t =>
                            t`<p>We see <span class="example-definition">${who}</span></p>`)
                            .setParent('#main');
                    }
                }
            }
        }
    }
}
