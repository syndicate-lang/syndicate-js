import resolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import sourcemaps from 'rollup-plugin-sourcemaps';

export class SyndicateRollup {
  constructor(packageName, options = {}) {
    this.packageName = packageName;
    this.globalName = options.globalName;
    this.configs = [];
  }

  distfile(insertion) {
    const f = `${this.packageName}${insertion}.js`;
    return `dist/${f}`;
  }

  terser() {
    return terser();
  }

  get umd() {
    return (insertion, extra) => ({
      file: this.distfile(insertion),
      format: 'umd',
      sourcemap: true,
      ... (this.globalName !== void 0) && { name: this.globalName },
      ... (extra || {})
    });
  }

  get es6() {
    return (insertion, extra) => ({
      file: this.distfile('.es6' + insertion),
      format: 'es',
      sourcemap: true,
      ... (extra || {})
    });
  }

  config(inputFile, outputMaker, options) {
    return {
      input: inputFile,
      plugins: [
        sourcemaps(),
        resolve({
          preferBuiltins: false,
          ... options && options.resolve
          }),
          ... (options && options.inputPlugins) || []
        ],
      output: [
        outputMaker('', options && options.output),
        outputMaker('.min', { plugins: [terser()], ... options && options.output }),
      ],
      ... options && options.external && { external: options.external },
    };
  }

  configNoCore(inputFile, outputMaker, options) {
    options = { ... options };
    options.output = { ... options.output };
    options.output.globals = { ... options.output.globals };
    if (!('@syndicate-lang/core' in options.output.globals))
      options.output.globals['@syndicate-lang/core'] = 'Syndicate';
    options.external = options.external || [];
    options.external.push('@syndicate-lang/core');
    return this.config(inputFile, outputMaker, options);
  }
}
